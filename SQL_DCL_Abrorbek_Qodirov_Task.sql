CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE postgres TO rentaluser;
GRANT SELECT ON TABLE customer TO rentaluser;
SELECT * FROM customer;
CREATE GROUP rental;
GRANT INSERT, UPDATE ON TABLE rental TO rental;
ALTER GROUP rental ADD USER rentaluser;
INSERT INTO rental (rental_id, rental_date, inventory_id, customer_id, return_date, staff_id, last_update ) VALUES (1001, '2023-11-27', 1234, 5678, NULL, 1, CURRENT_TIMESTAMP);
UPDATE rental SET column_name = new_value WHERE rental_id = existing_id;
REVOKE INSERT ON TABLE rental FROM rental;
DO $$
DECLARE
    customer_id INTEGER;
    first_name VARCHAR(50);
    last_name VARCHAR(50);
    role_name VARCHAR(100);
BEGIN
    FOR customer_record IN
        SELECT customer_id, first_name, last_name FROM customer WHERE payment_id IS NOT NULL AND rental_id IS NOT NULL
    LOOP
        customer_id := customer_record.customer_id;
        first_name := customer_record.first_name;
        last_name := customer_record.last_name;
        role_name := 'client_' || first_name || '_' || last_name;

        EXECUTE 'CREATE ROLE ' || role_name;
        EXECUTE 'GRANT USAGE ON SCHEMA public TO ' || role_name;
        EXECUTE 'GRANT SELECT ON TABLE rental TO ' || role_name;
        EXECUTE 'GRANT SELECT ON TABLE payment TO ' || role_name;
        EXECUTE 'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO ' || role_name;

        EXECUTE 'GRANT ' || role_name || ' TO rentaluser';
    END LOOP;
END $$;
SELECT * FROM rental WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'ab' AND last_name = 'qa');
SELECT * FROM payment WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'ab' AND last_name = 'qa');
